ESLint
------
Lint, or a linter, is a tool that analyzes source code to flag programming errors, bugs, 
stylistic errors, and suspicious constructs.[1] The term originates from a Unix utility that examined 
C language source code.[2]


ESLint is an open source JavaScript linting utility originally created by Nicholas C. Zakas in June 2013. 
Code linting is a type of static analysis that is frequently used to find problematic patterns or code that 
doesn’t adhere to certain style guidelines. There are code linters for most programming languages, and compilers 
sometimes incorporate linting into the compilation process.

JavaScript, being a dynamic and loosely-typed language, is especially prone to developer error. Without the 
benefit of a compilation process, JavaScript code is typically executed in order to find syntax or other errors. 
Linting tools like ESLint allow developers to discover problems with their JavaScript code without executing it.

The primary reason ESLint was created was to allow developers to create their own linting rules. ESLint is designed 
to have all rules completely pluggable. The default rules are written just like any plugin rules would be. 
They can all follow the same pattern, both for the rules themselves as well as tests. While ESLint will ship 
with some built-in rules to make it useful from the start, you’ll be able to dynamically load rules at any 
point in time.