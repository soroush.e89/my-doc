var fs = require('fs');

function readFiles(dirname, onFileContent, onError) {
  fs.readdir(dirname, function(err, filenames) {
    if (err) {
      onError(err);
      return;
    }
    filenames.forEach(function(filename) {
	  if(filename.includes("js")) return;
      fs.readFile(dirname + '/'+filename, 'utf-8', function(err, content) {
        if (err) {
          onError(err);
          return;
        }
		if(content.includes('<defs>')) return;
		let newContent = content.replace("</svg>", " <defs>"
	  +"<style>"
         +".fill-white{fill:#fff}.opacity-0{opacity:0}"
        +"</style>"
   +"</defs>"
	+"</svg>"
	)
		fs.writeFileSync(dirname + '/'+filename, newContent);
		
        onFileContent(filename, 'OK');
      });
    });
  });
}

readFiles('../Icons', function(filename, content) {

console.log(filename+'-->\r\n'+content)
}, function(err) {
  throw err;
});